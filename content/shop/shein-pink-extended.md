+++
title = "Extended pink knitted "
description = "Primordial brings you an extended version of our special pink top. Make you feel special with primordial's tops."
slug = "extended-shein"

[extra]
name = "Extended shein"
picked = false
price = "$40 (AUD)"
cover_image = "/images/items/extended-shein-01.jpg"
tags = ["2021", "summer-wear", "upper", "pink", "sleeve-less"]

[[extra.info]]
property_name = "Color"
property_value = "Pink"

[[extra.info]]
property_name = "Design"
property_value = "Backless"

[[extra.info]]
property_name = "Custom Order"
property_value = "Accepted"

[[extra.info]]
property_name = "Lace"
property_value = "With & Without"
+++
