+++
title = " Slim Slit Knit Cami Top"
description = " Slim Slit Knit Cami Top in Green"
slug = " Slim-Slit, Knit, Cami -Top"

[extra]
name = " Slim-Slit Top"
price = "$35(AUD)"
tags = ["Slim-Slit Knit Cami Top"]
cover_image = "/images/items/green.jpg"
picked = false

[[extra.info]]
property_name = "color"
property_value = "Green,blue"

[[extra.info]]
property_name = "Size"
property_value = "Small,Medium"

[[extra.info]]
property_name = "Design"
property_value = "Slim Slit "

[[extra.info]]
property_name = "Bulk Order"
property_value = "Accepted"
+++
