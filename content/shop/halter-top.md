+++
title = "Halter top"
description = "This time shine with halter tops. Perfect choice for your summer."
slug = "halter-top-green"

[extra]
cover_image = "/images/items/pruple.jpg"
name = "Halter Top"
price = "$35 (AUD)"
tags = ["halter", "tops", "purple", "open", "summer"]
picked = false

[[extra.info]]
property_name = "color"
property_value = "Purple"

[[extra.info]]
property_name = "Sizes"
property_value = "Large, Medium"

[[extra.info]]
property_name = "Design"
property_value = "Backless"

[[extra.info]]
property_name = "Custom Order"
property_value = "Accepted"
+++
