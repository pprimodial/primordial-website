+++
title = "Halter green neck in all sizes"
description = "This summer wear halter neck from primordial to shine and shine everywhere. Available in all colour with major green"
slug = "halter-neck-green"

[extra]
name = "Top with Skirt"
picked = false
price = "$65 (AUD)"
cover_image = "/images/items/green-halter-neck-01.jpg"
tags = ["summer", "crop", "green", "open", "skirt", "one-piece"]

[[extra.info]]
property_name = "Color"
property_value = "Green,Pink,"

[[extra.info]]
property_name = "Size"
property_value = "Medium/  Large"

[[extra.info]]
property_name = "Custom Order"
property_value = "Accepted"
+++
