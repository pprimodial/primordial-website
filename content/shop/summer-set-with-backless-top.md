+++
title = "Top with Ruffle Edge"
description = "Broderie crop top with ruffle edge."
slug = "broderie-crop-ruffle-edge "

[extra]
cover_image = "/images/items/tops.jpg"
name = "Ruffle Edge"
tags = ["Broderie", "crop-top", "ruffle", "edge."]
price = "$40(AUD)"
picked = true

[[extra.info]]
property_name = "Color"
property_value = "White, Silver"

[[extra.info]]
property_name = "sizes"
property_value = "Large, Medium"

[[extra.info]]
property_name = "Design"
property_value = "Ruffle edge"

[[extra.info]]
property_name = "Bulk Order"
property_value = "Available"

[[extra.info]]
property_name = "Variants"
property_value = "Cropped & Uncropped"
+++
