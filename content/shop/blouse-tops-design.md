+++
title = " Blouse design Tops"
description = "The tops can be wear with 2 different and unique styles.\nWe brings you tops with touch of blouse design. You can wear with two different unique styles."
slug = "blouse-designed-tops"

[extra]
cover_image = "/images/items/red.jpg"
price = "$45 (AUD)"
tags = ["two-in-one", "blouse", "knitted", "handmade", "red", "tops", "open"]
name = " Blouse design Tops"
picked = true

[[extra.info]]
property_name = "Color"
property_value = "Red,White"

[[extra.info]]
property_name = "Sizes"
property_value = "Large,Medium"

[[extra.info]]
property_name = "Design"
property_value = "Backless, half sleeve"

[[extra.info]]
property_name = "Custom order"
property_value = "Accepted"
+++
