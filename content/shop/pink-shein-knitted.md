+++
title = "Mixed Colour Crop Tube-Top"
description = "Handmade crop-top with the mixed colours of white and baby pink."
slug = "mixed-color-crop-top"

[extra]
name = "Pink Tube"
picked = true
price = "$40 (AUD)"
cover_image = "/images/items/knitted-pink-shein-01.jpg"
tags = ["summerset", "pink"]

[[extra.info]]
property_name = "Color"
property_value = "Pink"

[[extra.info]]
property_name = "Size"
property_value = "Small/ Medium/Large"

[[extra.info]]
property_name = "Custom Order"
property_value = "Accepted"

[[extra.info]]
property_name = "Bulk Order"
property_value = "Available"
+++
